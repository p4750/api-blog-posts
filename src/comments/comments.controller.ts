import { Body, Controller, Delete, Get, Param, Patch, Post, Put, UsePipes, ValidationPipe } from '@nestjs/common';
import { CommentsService } from './comments.service';
import { AtualizarCommentParcialDto } from './dtos/atualizar-comment-parcial.dto';
import { AtualizarCommentDto } from './dtos/atualizar-comment.dto';
import { CriarCommentDto } from './dtos/criar-comment.dto';
import { intComment } from './interfaces/comment.interface';

@Controller('api/comments')
export class CommentsController {

    constructor(private readonly commentsService: CommentsService) {}

    @Post()
    @UsePipes(ValidationPipe)
    async criarComment(
        @Body() criarCommentDto: CriarCommentDto): Promise<intComment> {
        return await this.commentsService.criarComment(criarCommentDto)
    }

    @Get()
    async consultarTodosComments(): Promise<intComment[]> {
        return await this.commentsService.consultarTodosComments()
    }

    @Get('/:_id')
    async consultarCommentPeloId(
        @Param('_id') _id: string): Promise<intComment> {
        return await this.commentsService.consultarCommentPeloId(_id)
    }

    @Put('/:_id')
    @UsePipes(ValidationPipe)
    async atualizarComment(
        @Body() atualizarCommentDto: AtualizarCommentDto,
        @Param('_id') _id: string): Promise<void> {
        await this.commentsService.atualizarComment(_id, atualizarCommentDto)
    }

    @Patch('/:_id')
    @UsePipes(ValidationPipe)
    async atualizarCommentParcial(
        @Body() atualizarCommentParcialDto: AtualizarCommentParcialDto,
        @Param('_id') _id: string): Promise<void> {
        await this.commentsService.atualizarComment(_id, atualizarCommentParcialDto)
    }

    @Delete('/:_id')
    async deletarComment(
        @Param('_id') _id: string): Promise<void> {
        await this.commentsService.deletarComment(_id)
    }

}
