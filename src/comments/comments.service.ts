import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { PostsService } from 'src/posts/posts.service';
import { AtualizarCommentDto } from './dtos/atualizar-comment.dto';
import { CriarCommentDto } from './dtos/criar-comment.dto';
import { intComment } from './interfaces/comment.interface';

@Injectable()
export class CommentsService {

    constructor(
        @InjectModel('Comment') private readonly commentModel: Model<intComment>,
        private readonly postsService: PostsService) {}

    async criarComment(criarCommentDto: CriarCommentDto): Promise<intComment> {
        await this.postsService.consultarPostPeloId(criarCommentDto.postId)
        const commentCriado = new this.commentModel(criarCommentDto)
        return await commentCriado.save()
    }

    async consultarTodosComments(): Promise<intComment[]> {
        return await this.commentModel.find().exec()
    }

    async consultarCommentPeloId(_id: string): Promise<intComment> {
        const commentEncontrado = await this.commentModel.findOne({_id}).exec()
        if (!commentEncontrado) {
            throw new NotFoundException(`Comment com id ${_id} não encontrado!`)
        }
        return commentEncontrado
    }

    async atualizarComment(_id: string, atualizarCommentDto: AtualizarCommentDto): Promise<void> {
        const commentEncontrado = await this.commentModel.findOne({_id}).exec()
        if (!commentEncontrado) {
            throw new NotFoundException(`Comment com id ${_id} não encontrado!`)
        }
        await this.commentModel.findOneAndUpdate({_id}, {$set: atualizarCommentDto}).exec()
    }

    async deletarComment(_id: string): Promise<void> {
        const commentEncontrado = await this.commentModel.findOne({_id}).exec()
        if (!commentEncontrado) {
            throw new NotFoundException(`Comment com id ${_id} não encontrado!`)
        }
        await this.commentModel.deleteOne({_id}).exec()
    }

}
