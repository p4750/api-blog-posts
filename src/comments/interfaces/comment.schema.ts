import * as mongoose from 'mongoose';

export const CommentSchema = new mongoose.Schema({

    postId: { type: mongoose.Schema.Types.ObjectId, ref: 'Post' },
    name: { type: String },
    email: { type: String },
    body: { type: String }}, { timestamps: true, collection: 'comments' }
    
)
