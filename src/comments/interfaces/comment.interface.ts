import { Document } from 'mongoose';
import { intPost } from 'src/posts/interfaces/post.interface';

export interface intComment extends Document {

    readonly postId: intPost
    readonly name: string
    readonly email: string
    readonly body: string

}
