import { IsNotEmpty, IsString } from 'class-validator';
import { intPost } from 'src/posts/interfaces/post.interface';

export class CriarCommentDto {

    @IsNotEmpty()
    readonly postId: intPost

    @IsNotEmpty()
    @IsString()
    readonly name: string

    @IsNotEmpty()
    @IsString()
    readonly email: string

    @IsNotEmpty()
    @IsString()
    readonly body: string

}
