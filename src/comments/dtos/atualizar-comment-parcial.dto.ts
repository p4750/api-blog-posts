import { IsOptional, IsString } from 'class-validator';

export class AtualizarCommentParcialDto {

    @IsOptional()
    @IsString()
    readonly name: string

    @IsOptional()
    @IsString()
    readonly email: string

    @IsOptional()
    @IsString()
    readonly body: string

}
