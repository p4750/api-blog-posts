import { IsNumber, IsOptional, IsString } from 'class-validator';

export class AtualizarPostParcialDto {

    @IsOptional()
    @IsNumber()
    readonly userId: number

    @IsOptional()
    @IsString()
    readonly title: string

    @IsOptional()
    @IsString()
    readonly body: string

}
