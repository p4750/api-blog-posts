import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class CriarPostDto {

    @IsNotEmpty()
    @IsNumber()
    readonly userId: number

    @IsNotEmpty()
    @IsString()
    readonly title: string

    @IsNotEmpty()
    @IsString()
    readonly body: string

}
