import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class AtualizarPostDto {

    @IsNotEmpty()
    @IsNumber()
    readonly userId: number

    @IsNotEmpty()
    @IsString()
    readonly title: string

    @IsNotEmpty()
    @IsString()
    readonly body: string

}
