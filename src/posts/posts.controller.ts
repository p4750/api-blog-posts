import { Body, Controller, Delete, Get, Logger, Param, Patch, Post, Put, Query, UsePipes, ValidationPipe } from '@nestjs/common';
import { intComment } from 'src/comments/interfaces/comment.interface';
import { AtualizarPostParcialDto } from './dtos/atualizar-post-parcial.dto';
import { AtualizarPostDto } from './dtos/atualizar-post.dto';
import { CriarPostDto } from './dtos/criar-post.dto';
import { intPost } from './interfaces/post.interface';
import { PostsService } from './posts.service';

@Controller('api/posts')
export class PostsController {

    constructor(private readonly postsService: PostsService) {}

    @Post()
    @UsePipes(ValidationPipe)
    async criarPost(
        @Body() criarPostDto: CriarPostDto): Promise<intPost> {
        return await this.postsService.criarPost(criarPostDto)
    }

    @Get()
    async consultarTodosPosts(
        @Query('with_text') texto: string): Promise<intPost[]> {
        return texto ? await this.postsService.consultarTodosPostsWithText(texto)
        : await this.postsService.consultarTodosPosts()
    }
    
    @Get('/:_id')
    async consultarPostPeloId(
        @Param('_id') _id: string,
        @Query('with_comments') withComments: any): Promise<any> {
        if (withComments == null) {
            return await this.postsService.consultarPostPeloId(_id)
        } else {
            return await this.postsService.consultarPostPeloIdWithComments(_id)
        }
    }

    @Get('/:_id/comments')
    async consultarCommentsPeloPostId(
        @Param('_id') _id: string): Promise<intComment[]> {
        return await this.postsService.consultarCommentsPeloPostId(_id)
    }

    @Put('/:_id')
    @UsePipes(ValidationPipe)
    async atualizarPost(
        @Body() atualizarPostDto: AtualizarPostDto,
        @Param('_id') _id: string): Promise<void> {
        await this.postsService.atualizarPost(_id, atualizarPostDto)
    }

    @Patch('/:_id')
    @UsePipes(ValidationPipe)
    async atualizarPostParcial(
        @Body() atualizarPostParcialDto: AtualizarPostParcialDto,
        @Param('_id') _id: string): Promise<void> {
        await this.postsService.atualizarPost(_id, atualizarPostParcialDto)
    }

    @Delete('/:_id')
    async deletarPost(
        @Param('_id') _id: string): Promise<void> {
        await this.postsService.deletarPost(_id)
    }

}
