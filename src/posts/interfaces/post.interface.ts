import { Document } from 'mongoose';

export interface intPost extends Document {

    readonly userId: number
    readonly title: string
    readonly body: string

}
