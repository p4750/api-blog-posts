import * as mongoose from 'mongoose';

export const PostSchema = new mongoose.Schema({

    userId: { type: Number },
    title: { type: String },
    body: { type: String }}, { timestamps: true, collection: 'posts' }
    
)
