import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { intComment } from 'src/comments/interfaces/comment.interface';
import { AtualizarPostDto } from './dtos/atualizar-post.dto';
import { CriarPostDto } from './dtos/criar-post.dto';
import { intPost } from './interfaces/post.interface';
import * as mongoose from 'mongoose';

@Injectable()
export class PostsService {

    constructor(
        @InjectModel('Post') private readonly postModel: Model<intPost>,
        @InjectModel('Comment') private readonly commentModel: Model<intComment>) {}

    async criarPost(criarPostDto: CriarPostDto): Promise<intPost> {
        const postCriado = new this.postModel(criarPostDto)
        return await postCriado.save()
    }

    async consultarTodosPosts(): Promise<intPost[]> {
        return await this.postModel.find().exec()
    }

    async consultarTodosPostsWithText(texto: string): Promise<intPost[]> {
        return await this.postModel.find({ $or: [{ title: { $regex: '.*' + texto + '.*', $options: 'i' } },
        { body: { $regex: '.*' + texto + '.*', $options: 'i' } }] }).exec()
    }

    async consultarPostPeloId(_id: any): Promise<intPost> {
        const postEncontrado = await this.postModel.findOne({_id}).exec()
        if (!postEncontrado) {
            throw new NotFoundException(`Post com id ${_id} não encontrado!`)
        }
        return postEncontrado
    }

    async consultarPostPeloIdWithComments(_id: string): Promise<intPost[]> {
        const postEncontrado = await this.postModel.findOne({_id}).exec()
        if (!postEncontrado) {
            throw new NotFoundException(`Post com id ${_id} não encontrado!`)          
        }
        return await this.postModel.aggregate([
            {
                $lookup: {
                    from: 'comments',
                    localField: '_id',
                    foreignField: 'postId',
                    as: 'comments'
                }    
            },    
            {
                $match: {
                    '_id': new mongoose.Types.ObjectId(_id) 
                }
            },
            {
                $project: {
                    userId: 1,
                    title: 1,
                    body: 1,
                    comments: 1
                }
            }
        ]).exec()
    }

    async consultarCommentsPeloPostId(_id: any): Promise<intComment[]> {
        const postEncontrado = await this.postModel.findOne({_id}).exec()
        if (!postEncontrado) {
            throw new NotFoundException(`Post com id ${_id} não encontrado!`)          
        }
        return await this.commentModel.find().where('postId').in(_id).exec()
    }

    async atualizarPost(_id: string, atualizarPostDto: AtualizarPostDto): Promise<void> {
        const postEncontrado = await this.postModel.findOne({_id}).exec()
        if (!postEncontrado) {
            throw new NotFoundException(`Post com id ${_id} não encontrado!`)
        }
        await this.postModel.findOneAndUpdate({_id}, {$set: atualizarPostDto}).exec()
    }

    async deletarPost(_id: string): Promise<void> {
        const postEncontrado = await this.postModel.findOne({_id}).exec()
        if (!postEncontrado) {
            throw new NotFoundException(`Post com id ${_id} não encontrado!`)          
        }
        await this.postModel.deleteOne({_id}).exec()
    }

}
