import { Module } from '@nestjs/common';
import { PostsModule } from './posts/posts.module';
import { CommentsModule } from './comments/comments.module';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb+srv://admin:DhEh0uKcuXCvLfrG@cluster0.mncug.mongodb.net/blogposts?retryWrites=true&w=majority'),
    PostsModule,
    CommentsModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
